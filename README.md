# lop linux+openresty+php7.3 一键部署

#### 介绍
镜像集成centos+openresty+php7.3+swoole4.6 方便本地快速部署php开发环境

#openresty 目录 /usr/local/openresty
#php 目录 /usr/local/php



#### 安装教程

进入www目录
cd /var/www 

拉取代码
git clone https://gitee.com/k-huyi/lop.git

你的目录结构应该是这样

/var/www

html  lop

#### 使用说明

cd lop

启动容器
docker-compose up -d

配置多域名进入nginx/sites 添加配置即可
